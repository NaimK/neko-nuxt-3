import { createGtm } from '@gtm-support/vue-gtm'

export default defineNuxtPlugin((nuxtApp) => {
  const { public: { gtmId } } = useRuntimeConfig()

  nuxtApp.vueApp.use(
    createGtm({
      id: gtmId
    })
  )
})
