import svgLoader from 'vite-svg-loader'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  target: 'static',
  app: {
    pageTransition: { name: 'page', mode: 'out-in' }
  },
  components: {
    global: true,
    dirs: ['~/components']
  },
  content: {
    documentDriven: true
  },
  css: [
    'the-new-css-reset',
    '~/assets/css/main.pcss'
  ],
  modules: [
    '@nuxt/content'
  ],
  postcss: {
    plugins: {
      'postcss-mixins': {
        mixinsDir: './assets/css/mixins'
      },
      'postcss-nested': {}
    }
  },
  runtimeConfig: {
    // The private keys which are only available within server-side
    // apiSecret: '123',
    // Keys within public, will be also exposed to the client-side
    public: {
      gtmId: 'GTM-KCX9VH7'
    }
  },
  vite: {
    plugins: [
      svgLoader()
    ]
  }
})
