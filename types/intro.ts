export interface CardIntroType {
    image: string,
    title: string,
    text: string,
    url?: string
}
