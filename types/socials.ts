export interface SocialNetworkType {
    icon: string,
    url: string,
}
