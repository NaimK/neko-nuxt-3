export default defineNuxtRouteMiddleware((_, __) => {
  useNuxtApp().hook('page:finish', () => {
    if (history.state.scroll) {
      setTimeout(() => window.scrollTo(history.state.scroll), 400)
    } else {
      setTimeout(() => window.scrollTo(0, 0), 400)
    }
  })
})
